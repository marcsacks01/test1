| Application | PostgreSQL | Redis | Neo4j | MinIO | Rabbitmq |Atlas | 
| ----- | ------- | ----- | ----- | ----- | ----- | -----|
| Blue Brain Nexus |  |  |  |  |  |  |
| Fluentd    |  |  |  |  |  |  |
| Guacamole  | :heavy_check_mark: |  |  |  |  |  |
| Jupyterhub | :heavy_check_mark: |  |  |  |  |  |
| Kong       | :heavy_check_mark: |  |  |  |  |  |
| Keycloak   | :heavy_check_mark: |  |  |  |  |   |
| Superset   | :heavy_check_mark: |  |  |  |  |  |
| XWiki      | :heavy_check_mark: |  |  |  |  |  |

